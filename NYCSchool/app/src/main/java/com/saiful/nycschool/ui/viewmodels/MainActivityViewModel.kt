package com.saiful.nycschool.ui.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.saiful.nycschool.model.BaseResponse
import com.saiful.nycschool.model.SchoolInfo
import com.saiful.nycschool.repository.ApiRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(private val repository: ApiRepository) :
    ViewModel() {

    // live data to observe from activity class
    val schoolInfoResult: MutableLiveData<BaseResponse<List<SchoolInfo>>> =
        MutableLiveData()

    // To get School Info
    fun getSchoolInfo() {
        schoolInfoResult.value = BaseResponse.Loading()
        viewModelScope.launch {
            try {
                val response = repository.getSchoolList()
                if (response.code() == 200) {
                    schoolInfoResult.value = BaseResponse.Success(response.body())
                } else {
                    schoolInfoResult.value = BaseResponse.Error(response.message())
                }
            } catch (ex: Exception) {
                schoolInfoResult.value = BaseResponse.Error(ex.message)
            }
        }
    }

}