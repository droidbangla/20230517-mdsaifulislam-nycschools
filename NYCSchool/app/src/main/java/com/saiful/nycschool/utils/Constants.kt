package com.saiful.nycschool.utils

object Constants {

    //NYC school data END_Point
    const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    const val SCHOOL_INFO = "school_info"
    const val NETWORK_CALL_TIMEOUT = 60L
}